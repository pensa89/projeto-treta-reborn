#projeto-treta-reborn
Projeto de objetivo acadêmico que consiste na construção de um jogo no Unity com C#

Grupo: José Carlos de Almeida, Pedro Ginnari e Vanessa Proença

Documentação:

SmoothCameraScript:

- Damp time: tempo de efeito do damp (?) da câmera.
- Target: transform que será seguido pela câmera; não pode ser nulo.

DamageOnCollision:

- Damage: dano causado numa colisão com outra entidade.
- OnCollisionEnter2D: método que causa o dano a outro objeto caso sua tag seja diferente do objeto rodando o script.

EnemyAutoFire: faz um objeto atacar com as propriedades de uma arma.

Health: script por cuidar dos Health Points das entidades.

- MaxHP: atributo responsável por definir o HP máximo.
- InitialHP: atributo responsável por definir o HP inicial.
- Hp: método usado pelas propriedades get e set de C# para manipulação de HP.

BasicEnemy: script responsável por forçar propriedades básicas de uma entidade inimiga.

Shot: script responsável por cuidar das entidades de projétil; não está sendo utilizada atualmente.

Weapon: script responsável por descrever propriedades de uma arma.

- FireRate: taxa em que a arma atira.
- Cooldown: taxa de recarregamento da arma.
- ShotPrefab: prefab que definirá o visual e as propriedades de um projétil; não pode ser nulo.

Player: script responsável por forçar propriedades básicas de uma entidade do jogador.

EnemyStraightShot: script responsável por projéteis com trajetórias retilíneas.

- Speed: atributo responsável por controlar a velocidade de um projétil.

FlipEnemyHorizontally: script bugado.

PlayerMovement: script responsável pelo movimento do jogador.

- MoveSpeed: atributo que controla a velocidade de locomoção do jogador.
- JumpSpeed: atributo que controla a velocidade de pulo do jogador.
- GroundCheck: gameObject vazio que controla o pulo duplo, não pode ser nulo.
- WhatIsGround: conjunto de layers usadas para controlar o pulo.

EnemyStraightShot: script responsável por projéteis com trajetórias que perseguem o jogador.

- ShotSpeed: atributo responsável por controlar a velocidade de um projétil.
- RotateSpeed: atributo responsável por controlar a velocidade de rotação de um projétil.

RotateScript: script responsável por rotacionar um objeto de modo que ele sempre "aponte" para um alvo.

- Target: transform que será seguido pelo gameObject que estiver rodando o script.
- Speed: atributo responsável por determinar a velocidade de rotação de um objeto.