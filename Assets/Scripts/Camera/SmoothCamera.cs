﻿using UnityEngine;
using System.Collections;

public class SmoothCamera : MonoBehaviour {

    [SerializeField] private float dampTime = 0.15f;
    [SerializeField] private Transform target;
    private bool canCameraFollow = true;
    private Vector3 velocity = Vector3.zero;
    private Camera smoothCamera;

    void Start()
    {
        smoothCamera = GetComponent<Camera>();
    }

    void FixedUpdate()
    {
        //If character goes back, camera cant follow (camera stays), =false, otherwise it follows, =true.
        //-- ps, Ginnari wanted to test terciary statement
       
        canCameraFollow = gameObject.transform.position.x < target.position.x ? true : false;

        //target is the player
        Vector3 point = smoothCamera.WorldToViewportPoint(target.position);
        Vector3 delta = target.position - smoothCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.2f, point.z));
        Vector3 destination = transform.position + delta;
        if (canCameraFollow)
        {
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
        }    
    }

    // to return value of "is camera following character?"
    public bool _canCameraFollow { get { return this.canCameraFollow; } }
}
