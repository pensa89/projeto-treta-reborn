﻿using UnityEngine;
using System.Collections;

public class LevelOneCreator : MonoBehaviour
{
    private Transform cameraTransform;
    //private float spriteWidth;
    private const float frameWidth = 19.2f;
    //The game objects that will be used to create the level
    private int switchDrawing = 0;

    [SerializeField]
    private GameObject[] bgLayer1;
    [SerializeField]
    private GameObject bgLayer2;
    [SerializeField]
    private GameObject[] floor;

    private float offSet = 0f;

    private int currentBG1 = 1;
    // points x and y to identify when and where to create a new element to the level
    private float frameSpotX;
    private float bgFrameSpotY;
    private float floorSpotY;


    //the current frame from the game, each frame is located at 19.2 units
    private int frameCounter = 0;
    // Use this for initialization

    void Start()
    {
        cameraTransform = Camera.main.transform;

        frameSpotX = 0f;
        bgFrameSpotY = 0.7f;
        floorSpotY = 1.0f;
        CreateStartScene();


    }



    // Update is called once per frame
    void Update()
    {
        if (Camera.main.transform.position.x >= frameSpotX)
        {
            frameSpotX += 19.2f;
            //GameObject.Find("BackGroundLayer1").GetComponent<BackGroundCreator>().CreateBGLayer1(frameSpotX,frameSpotY , frameCounter);
            //GameObject.Find("BackGroundLayer2").GetComponent<BackGroundCreator>().CreateBGLayer2(frameSpotX, frameSpotY);

            //need to create the compensation to the new bg speed
            CreateBGLayer1(frameSpotX, bgFrameSpotY, frameCounter);
            CreateBGLayer2(frameSpotX, bgFrameSpotY, frameCounter);
            CreateFloor(frameSpotX, floorSpotY, frameCounter);

            frameCounter++;

        }
    }

    //creates the start scene
    void CreateStartScene()
    {
        Instantiate(bgLayer1[0], new Vector2(0.0f, bgFrameSpotY), Quaternion.identity);
        Instantiate(bgLayer2, new Vector2(0.0f, bgFrameSpotY), Quaternion.identity);
        Instantiate(floor[0], new Vector2(0.0f, floorSpotY), Quaternion.identity);

    }

    //currentbgl1 is the current frame
    public void CreateBGLayer1(float x, float y, int currentBGL1)
    {
        if (currentBGL1 != 0)
        {
            switchDrawing = 10 % currentBGL1;
        }
        else
        {

            //ignore

        }


        if (verifyOffSet())
        {
            Debug.Log("reached the 11th");
        }
        else
        {
            currentBGL1 = (currentBGL1 + switchDrawing) % 2;
            //bg1layer1 = bg1Objects[0];
            //bg2layer1 = bg1Objects[1];

            if (currentBGL1 == 0)
            {
                Instantiate(bgLayer1[currentBGL1 + 1], new Vector2(x + offSet, y), Quaternion.identity);
            }
            else if (currentBGL1 == 1)
            {
                Instantiate(bgLayer1[currentBGL1 - 1], new Vector2(x + offSet, y), Quaternion.identity);
            }
            else
            {
                Debug.Log("not drawing");
            }

            offSet += 1.92f;
        }

    }

    private bool verifyOffSet()
    {
        if (offSet >= 19.2f)
        {
            offSet = 0.0f;
            return true;
        }
        else
        {
            return false;
        }
    }

    //currentBGL2 is the current frame
    public void CreateBGLayer2(float x, float y, int currentBGL2)
    {
        if ((currentBGL2 % 2) != 0)
        {
            Debug.Log("do not draw");

        }
        else
        {

            Instantiate(bgLayer2, new Vector2(x, y), Quaternion.identity);

        }

    }

    public void CreateFloor(float x, float y, int currentFloor)
    {
        currentFloor = currentFloor % 3;

        switch (currentFloor)
        {
            case 0:
                Instantiate(floor[currentFloor + 1], new Vector2(x, y), Quaternion.identity);
                break;
            case 1:
                Instantiate(floor[currentFloor + 1], new Vector2(x, y), Quaternion.identity);
                break;
            case 2:
                Instantiate(floor[currentFloor - 2], new Vector2(x, y), Quaternion.identity);
                break;

        }


    }


}

