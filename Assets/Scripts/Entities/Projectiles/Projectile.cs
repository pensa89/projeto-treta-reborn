﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(DamageOnCollision))]
[RequireComponent(typeof(Rigidbody2D))]
public class Projectile : MonoBehaviour {

    [SerializeField] private float projectileSpeed = 1f;

    public float _projectileSpeed
    {
        get { return projectileSpeed; }
    }

	protected virtual void Start ()
    {
        Destroy(gameObject, 20);
	}

    protected void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag != gameObject.tag)
        {
            Destroy(gameObject);
        }
    }
}
