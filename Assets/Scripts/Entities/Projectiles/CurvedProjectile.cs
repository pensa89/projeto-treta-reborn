﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(DamageOnCollision))]
[RequireComponent(typeof(Rigidbody2D))]
public class CurvedProjectile : Projectile {

	[SerializeField] private Transform target;
	[SerializeField] private float rotateSpeed = 1f;

    protected override void Start () 
	{
        base.Start();
    }

    protected void Update()
	{
		target.position = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<Transform> ().position;
		Vector3 vectorToTarget = target.position - transform.position;
		float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
		Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
		transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * rotateSpeed);
	}

    protected void FixedUpdate()
	{
		Vector2 moveDirection = new Vector2(_projectileSpeed, 0) * Mathf.Sign(transform.localScale.x) * Time.deltaTime;
		transform.Translate(moveDirection);
	}
}
