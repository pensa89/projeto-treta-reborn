﻿using UnityEngine;
using System.Collections;

public class StraightProjectile : Projectile {

    protected override void Start () 
	{
        base.Start();
	}

    protected void FixedUpdate()
	{
        Vector2 moveDirection = new Vector2(_projectileSpeed, 0) * Time.deltaTime;
		transform.Translate(moveDirection);
	}
}
