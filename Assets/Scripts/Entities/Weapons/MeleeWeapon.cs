﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(DamageOnCollision))]
public class MeleeWeapon : Weapon
{
    protected override void Update()
    {
        base.Update();
        if (Input.GetButtonDown("Fire1") && CanShoot)
        {
            //Play melee attack animation here.
        }
    }

}
