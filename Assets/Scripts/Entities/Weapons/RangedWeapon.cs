﻿using UnityEngine;
using System.Collections;

public class RangedWeapon : Weapon
{
    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
        if (Input.GetButtonDown("Fire1") && CanShoot)
        {
            Shoot(false);
        }
    }
}
