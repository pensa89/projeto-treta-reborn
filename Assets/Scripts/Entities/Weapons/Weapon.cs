﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {

	[SerializeField] private float fireRate = 2f;
	[SerializeField] private float cooldown = 1f;
	[SerializeField] private Transform shotPrefab;
    private float blockCoeficient = 0.8f;

    protected virtual void Start()
	{
		cooldown = 0f;
	}
	
	protected virtual void Update()
	{
		if (cooldown > 0)
			cooldown -= Time.deltaTime;
	}

    public float _blockCoeficient { get { return this.blockCoeficient;  } }

	public virtual void Shoot(bool isEnemy)
	{
        if (CanShoot)
        {
            cooldown = fireRate;
            Transform shotTransform = Instantiate(shotPrefab, transform.position, transform.rotation) as Transform;
        }
	}
	
	public bool CanShoot
	{
		get	{ return cooldown <= 0f; }
	}
}
