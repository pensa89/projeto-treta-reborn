﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(DamageOnCollision))]
[RequireComponent(typeof(Rigidbody2D))]
public class Shot : MonoBehaviour {

	[SerializeField] private float moveSpeed = 1f;

	void Start () 
	{
		Destroy(gameObject, 5); // 5 sec
	}

	void FixedUpdate()
	{
		Vector2 moveDirection = new Vector2(moveSpeed, 0) * Time.deltaTime;
		transform.Translate(moveDirection);
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		Destroy (gameObject);
	}
}
