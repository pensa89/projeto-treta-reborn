﻿using UnityEngine;
using System.Collections;

public class RotateScript : MonoBehaviour {

    //Target of the game object rotation.
	[SerializeField] private Transform target;
	[SerializeField] private float speed = 2f;

	void FixedUpdate () 
	{
		Vector3 vectorToTarget = target.position - transform.position;
		float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
		Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
		transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * speed);
	}
}
