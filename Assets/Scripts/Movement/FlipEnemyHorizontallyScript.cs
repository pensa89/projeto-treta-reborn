﻿using UnityEngine;
using System.Collections;

public class FlipEnemyHorizontallyScript : MonoBehaviour
{
    [SerializeField] private Transform target;
    private float targetDirectionSign;
    
    void Start()
    {
        // Faces the player
        Util.FacePointHorizontally(transform, target.position);
    }    

    protected void Update ()
    {
        Util.FacePointHorizontally(transform, target.position);
    }    
}
