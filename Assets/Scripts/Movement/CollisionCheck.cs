﻿using UnityEngine;
using System.Collections;

public class CollisionCheck : MonoBehaviour {

    [SerializeField] private Transform groundCheck;
    [SerializeField] private Transform rightWallCheck;
    [SerializeField] private Transform leftWallCheck;
    [SerializeField] private LayerMask whatIsGround;
    [SerializeField] private LayerMask whatIsWall;

    private float groundRadius = 1.0f;

    public bool checkIfJumping()
    {
        return Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
    }

    public bool checkIfWallJumping()
    {
        return Physics2D.OverlapCircle(rightWallCheck.position, groundRadius, whatIsWall) ||
            Physics2D.OverlapCircle(leftWallCheck.position, groundRadius, whatIsWall);
    }
}
