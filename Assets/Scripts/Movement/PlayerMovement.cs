﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
    [SerializeField] protected float moveSpeed;
    [SerializeField] protected float moveSpeedMax;
    [SerializeField] protected float jumpPushForceY;
    [SerializeField] protected float jumpPushForceX;
    [SerializeField] private Skills[] skills = new Skills[3];

    private float slideTime = 0;
    private float fightingPoseTime = 0;
    private float groundRadius = 0.5f;
    private bool moving = false;
    [SerializeField] private bool isJumping = false;
    private bool isSliding = false;
    [SerializeField] private bool isDoubleJumping = false;
    [SerializeField] private bool isWallJumping = false;
    private bool facingRight = true;
    private bool upsideDown = false;
    private Rigidbody2D body;
    private Animator anim;
    private CollisionCheck collisionCheck;

    // variables for new walk
    private Vector2 newposition;
    private float currentSpeed = 0;

    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        body = gameObject.GetComponent<Rigidbody2D>();
        collisionCheck = gameObject.GetComponent<CollisionCheck>();
    }

    protected void Update()
    {
        if (Input.GetButtonDown("Jump") && !isJumping)
        {
            Jump();
        }
        else if (Input.GetButtonDown("Jump") && !isDoubleJumping)
        {
            DoubleJump();
        }
        if (Input.GetButtonDown("Jump") && isJumping && isWallJumping)
        {
            WallJump();
        }

        

    }

    protected void FixedUpdate()
    {

        if (anim.GetBool("isSliding"))
        {
            slideTime += Time.deltaTime;
            Slide();
            if (slideTime >= 1)
            {
                setSlideOff();
            }
        }
        if (anim.GetBool("isFighting"))
        {
            fightingPoseTime += Time.deltaTime;
            if (fightingPoseTime >= 6)
            {
                DeactivateFightingPose();
            }
        }
        //not sliding and not walking

        else if (!anim.GetBool("isWalking"))
        {
            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                Blocking();
            }
            if (anim.GetBool("isBlocking") && Input.GetKeyUp(KeyCode.LeftControl))
            {
                Unblocking();
            }
        }

        // walk if not above
        //maybe jump code should be here
        if (!(anim.GetBool("isBlocking") || anim.GetBool("isSliding")))
        {
            isJumping = !collisionCheck.checkIfJumping();
            isWallJumping = collisionCheck.checkIfWallJumping();
            Walk();
            Debug.Log(isJumping);
        }



    }
    private void Walk()
    {
        float moveInput = Input.GetAxis("Horizontal");
        if (moveInput != 0)
        {
            moving = true;
            anim.SetBool("isWalking", true);
            newposition = gameObject.transform.position;


            /*
                        Vector2 jumpForce = new Vector2(moveSpeed * Mathf.Sign(moveInput), 0);
                        body.AddForce(jumpForce);
                        body.velocity = Vector2.ClampMagnitude(body.velocity, moveSpeedMax);

                */


            // if moving left
            if (moveInput > 0)
            {
                currentSpeed = moveSpeed;
                newposition.x = newposition.x + currentSpeed;
                
            }
            //if moving right
            else
            {
                currentSpeed = (-1) * moveSpeed;
                newposition.x = newposition.x + currentSpeed;
                

            }
            
            gameObject.transform.position = newposition;

            if (Input.GetKeyDown(KeyCode.C))
            {
                //isSliding = true;
                Slide();

            }

        }
        // If Xitah is not moving.
        else
        {
            moving = false;
            currentSpeed = 0.0f;
            anim.SetBool("isWalking", false);

            // Xitah can only crouch if she is not moving.
            if (Input.GetKeyDown(KeyCode.S))
            {
                Crouch();
            }
            else
            {
                setCrouchOff();
            }
        }

        if (moveInput > 0 && !facingRight)
            FlipHorizontally();
        if (moveInput < 0 && facingRight)
            FlipHorizontally();
    }
    private void Slide()
    {

        //float moveInput = Input.GetAxis("Horizontal");
        anim.SetBool("isSliding", true);

        //body.velocity = Vector2.ClampMagnitude(body.velocity, moveSpeedMax - slideTime);
        if (currentSpeed < 0)
        {
            currentSpeed += 0.0015f;

        }else
        {
            currentSpeed -= 0.0015f;

        }
        newposition.x = newposition.x + currentSpeed;

        //maybe create a method for readability
        gameObject.transform.position = newposition;



    }
    private void Blocking()
    {

        //float moveInput = Input.GetAxis("Horizontal");
        anim.SetBool("isBlocking", true);

    }

    private void Unblocking()
    {
        Debug.Log("unblocked");
        //float moveInput = Input.GetAxis("Horizontal");
        anim.SetBool("isBlocking", false);

    }



    private void setSlideOff()
    {
        anim.SetBool("isSliding", false);
        isSliding = false;
        slideTime = 0;
    }

    private void FlipHorizontally()
    {
        facingRight = !facingRight;
        transform.Rotate(transform.rotation.x, transform.rotation.y + (Mathf.Sign(transform.localScale.x) * 180), transform.rotation.z);
    }

    private void FlipVertically()
    {
        upsideDown = !upsideDown;
        transform.Rotate(transform.rotation.x, transform.rotation.y, transform.rotation.z + (Mathf.Sign(transform.localScale.y) * 90));
    }

    protected virtual void Jump()
    {
        isJumping = true;
        isDoubleJumping = false;
        Vector2 jumpForce = Vector2.up * jumpPushForceY;
        body.AddForce(jumpForce);

        //Need to be changed to the attack line code
        ActivateFightingPose();
    }

    protected virtual void DoubleJump()
    {
        isDoubleJumping = true;
        Vector2 jumpForce = Vector2.up * jumpPushForceY;
        body.AddForce(jumpForce);
    }

    protected virtual void WallJump()
    {
        FlipHorizontally();
        Vector2 jumpForce = new Vector2(jumpPushForceX * -Mathf.Sign(transform.localScale.x), jumpPushForceY * Mathf.Sign(body.gravityScale));
        body.AddForce(jumpForce);
    }

    protected virtual void Crouch()
    {
        anim.SetBool("isCrouching", true);
    }

    protected virtual void setCrouchOff()
    {
        anim.SetBool("isCrouching", false);
    }

    public float getMoveSpeed()
    {
        return currentSpeed;
    }
    private void ActivateFightingPose()
    {
        anim.SetBool("isFighting", true);
    }
    private void DeactivateFightingPose()
    {
        anim.SetBool("isFighting", false);
        fightingPoseTime = 0;
    }

    public void ToggleFightingPose()
    {
        anim.SetBool("isFighting", !anim.GetBool("isFighting"));

    }
}
