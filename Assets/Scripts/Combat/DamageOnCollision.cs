﻿using UnityEngine;
using System.Collections;

public class DamageOnCollision : MonoBehaviour {

	[SerializeField] private int damage = 1;
    [SerializeField] private int knockbackX = 25;
    [SerializeField] private int knockbackY = 30;

    protected virtual void OnCollisionEnter2D(Collision2D other) 
	{
		Health otherHealth = other.gameObject.GetComponent<Health> ();

        Block otherBlock = other.gameObject.GetComponent<Block>();

        //need to add the facing variable to this, but need the enemy and player's facing, or better, to avoid bugs, verify if it hit player's back
        if (otherHealth != null && other.gameObject.tag != gameObject.tag)
            if (other.gameObject.GetComponent<Animator>().GetBool("isBlocking")) {
                otherBlock.DamageFilter(otherHealth.Damage(damage));
            }
            else
            {
                otherHealth.Damage(damage);
            }
			
	}
}
