﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Weapon))]
public class EnemyAutoFire : MonoBehaviour {

	private Weapon weapon;
	
	void Awake()
	{
		weapon = GetComponent<Weapon>();
	}
	
	void Update()
	{
		if (weapon != null && weapon.CanShoot)
			weapon.Shoot(true);
	}
}
