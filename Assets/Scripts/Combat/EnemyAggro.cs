﻿using UnityEngine;
using System.Collections;

public class EnemyAggro : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private Transform target;
    private Vector3 initialPosition;
    private bool isAggroed = false;
    private Rigidbody2D body;

    void Start ()
    {
        body = GetComponent<Rigidbody2D>();

        // Stores the spawn position
        initialPosition = transform.position;
        // Faces the player
        Util.FacePointHorizontally(transform, target.position);
    }
	
	void Update ()
    {        
        if (isAggroed)
        {
            Util.FacePointHorizontally(transform, target.position);
            body.velocity = transform.right * speed;
        } else
        {
            // If target is out of range, stands still.
            body.velocity = new Vector3(0, body.velocity.y, 0);
            body.angularVelocity = 0f;

            // If target is out of range, goes back to spawn point.
            //Util.FacePointHorizontally(transform, initialPosition);
            //body.velocity = transform.right * speed * 0.3f;
        }
	}

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            isAggroed = true;
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            isAggroed = false;
        }
    }

}
