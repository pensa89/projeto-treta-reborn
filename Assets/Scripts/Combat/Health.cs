﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

	[SerializeField] private int maxHP = 10;
    private int initialHP = 1;
    private int currentHP = 0;
    

    protected void Start()
    {
        initialHP = maxHP;
        currentHP = initialHP;
        
    }

	public int currentHealth
    {
		get { return currentHP;}
		set
		{
			currentHP = Mathf.Clamp(value, 0, maxHP);
			if(currentHP == 0)
            {
                OnZeroHP();
            }				
		}
	}

    public int maxHealth
    {
        get { return maxHP; }
    }

	public void Reset()
	{
		currentHealth = initialHP;
	}

	public int Damage(int damageCount)
	{
        
		currentHealth -= damageCount;
        return damageCount;
	}

    public void Heal(int healCount)
    {
        currentHealth += healCount;
    }

	protected virtual void OnZeroHP()
	{
		Destroy(gameObject);
	}
}
