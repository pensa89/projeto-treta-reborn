﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour {

    private int blockCounter;
    


    // Use this for initialization
    void Start () {
        blockCounter = 0;
        
    }
	
	// Update is called once per frame
	void Update () {
        

    }

    public int DamageFilter(int dmg) {

        //        whole damage * ((nr# of blocks * (shield defense (0~1] * random number [0,1]))
        int newDmg = (int)((float)dmg * ((blockCounter) * (gameObject.GetComponent<Weapon>()._blockCoeficient * Random.Range(0.1f,1.0f) )));

        if (newDmg > dmg)
        {
            return dmg;
        }
        else{

            blockCounter++;
            Debug.Log("Blocked");
            return newDmg;
        }

    }
}
