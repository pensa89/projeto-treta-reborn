﻿using UnityEngine;
using System.Collections;

public abstract class Item : MonoBehaviour
{
    protected int quantity;
    protected int maxQuantity;

    public virtual void Awake ()
    {
        quantity = 0;
	}

    public int _quantity { get { return quantity; } set { this.quantity = value; } }

    public int _maxQuantity { get { return maxQuantity; } set { this.maxQuantity = value; } }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            Inventory inventory = other.GetComponent<Inventory>();
            inventory.addItem(this);     
        }
    }

    public abstract void Use();
}
