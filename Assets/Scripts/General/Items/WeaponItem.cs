﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponItem : Item
{
    [SerializeField] private Weapon weapon;
    private bool active;
    private int limit = 1;
    

    public override void Awake()
    {
        base.Awake();
        active = false;
    }

    public bool _active { get { return this.active; } set { this.active = value; } }
            
    
    public float getBlockCoef() { 
        return weapon._blockCoeficient;
    }
    
    public void equipWeapon()
    {
        WeaponItem[] weapons = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<Inventory>().getWeapons();
        for (int i = 0; i < weapons.Length; i++)
        {
            if (weapons[i] != null && weapons[i]._active)
            {
                weapons[i].unequipWeapon();
            }
        }
        _active = true;
    }

    public void unequipWeapon()
    {
        _active = false;
    }

    public override void Use()
    {
        if(_active)
        {
            weapon.Shoot(false);
        }
    }

}