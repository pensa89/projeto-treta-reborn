﻿using UnityEngine;
using System.Collections;

public class ConsumableItem : Item
{
    public override void Awake()
    {
        base.Awake();
    }

    public override void Use()
    {
        applyEffects();
        Inventory inventory = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<Inventory>();
        inventory.removeItem(this);
    }

    public void applyEffects()
    {
        //Apply item effects.
    }
}
