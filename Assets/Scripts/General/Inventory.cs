﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Inventory : MonoBehaviour
{
    [SerializeField]
    private int maxNumberOfWeapons = 5;
    [SerializeField]
    private int maxNumberOfConsumables = 3;
    private WeaponItem[] weapons;
    private ConsumableItem[] consumables;
    private WeaponItem equippedWeapon1;
    private WeaponItem equippedWeapon2;


    public void Awake()
    {
        weapons = new WeaponItem[maxNumberOfWeapons];
        consumables = new ConsumableItem[maxNumberOfConsumables];
        equippedWeapon1 = new WeaponItem();
        equippedWeapon2 = new WeaponItem();
    }

    public WeaponItem[] getWeapons()
    {
        return weapons;
    }

    public ConsumableItem[] getConsumables()
    {
        return consumables;
    }

    public int getMaxNumberOfWeapons()
    {
        return maxNumberOfWeapons;
    }

    public int getMaxNumberOfConsumables()
    {
        return maxNumberOfConsumables;
    }


    public void addItem(Item item)
    {
        Item[] list = null;
        int limit = 0;

        if (item is ConsumableItem)
        {
            list = consumables;
            limit = maxNumberOfConsumables;
        }
        else if (item is WeaponItem)
        {
            list = weapons;
            limit = maxNumberOfWeapons;
        }

        int index = findFirstEmptySlot(list);

        if(index != -1 && item._quantity < item._maxQuantity)



        {
            item._quantity++;
            list[findFirstEmptySlot(list)] = item;
            Destroy(item.gameObject);
        }
    }

    public void removeItem(Item item)
    {
        Item[] list = null;

        if (item is ConsumableItem)
        {
            list = consumables;
        }
        else if (item is WeaponItem)
        {
            list = weapons;
        }

        int index = findItemSlot(list, item);
        item._quantity--;
        if (index != -1 && item._quantity <= 0)
        {            

            list[index] = null;
        }
    }

    private int findFirstEmptySlot(Item[] items)
    {
        for (int i = 0; i < items.Length; i++)
        {
            if (items.ElementAt(i) == null)
            {
                return i;
            }
        }
        return -1; //No slots avaliable.
    }

    private int findItemSlot(Item[] items, Item item)
    {
        for (int i = 0; i < items.Length; i++)
        {
            if (items.ElementAt(i) == item)
            {
                return i;
            }
        }
        return -1; //Item slot not found.
    }
}