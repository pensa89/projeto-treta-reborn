﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerHealth : MonoBehaviour
{
    /* For the correct functioning of this script the following
     * items must be attended:
     * 
     * - It must be inside a Image, which can be created through 
     * UI -> Image
     * - Event Systems can be deleted
     * - Set Canvas as Screen space - Camera
     * - Set Render Camera as the level's camera
     * - Image must have a valid Sprite.
     * - Image type must be filled and the fields Image Type must 
     * be Filled, Fill method must be Horizontal and Fill origin
     * must be left.
     */

    [SerializeField] private Image healthBarImage;

    private GameObject player;
    private Health playerHealth;
    private PlayerMovement playerMovement;
    private int currentHealth;
    private int maxHealth;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<Health>();
        playerMovement = player.GetComponent<PlayerMovement>();
        healthBarImage = gameObject.GetComponent<Image>();

        currentHealth = playerHealth.currentHealth;
        maxHealth = playerHealth.maxHealth;
    }

    void Update()
    {
        currentHealth = playerHealth.currentHealth;
        //fillAmount requires a value between 0 and 1.
        float ratio = (float) currentHealth / maxHealth;        
        healthBarImage.fillAmount = ratio;
    }	
}
