﻿using UnityEngine;
using System.Collections;

public abstract class Skills : MonoBehaviour {

    // Use this for initialization
    protected string skillName;
    protected string description;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    protected abstract void Effect();

    protected string getSkillName()
    {
        return skillName;
    }
    
    protected string getDescription()
    {
        return description;
    }
    
}
