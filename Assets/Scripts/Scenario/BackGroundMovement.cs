﻿using UnityEngine;
using System.Collections;

public class BackGroundMovement : MonoBehaviour {
    float movespeed;
    GameObject player;
    Transform bgPosition;
    Vector2 vec;
    float thisY;
    private Vector2 newposition;
    GameObject camera;
    // Use this for initialization
    void Start () {
        player = GameObject.Find("Player");
        camera = GameObject.Find("Main Camera");
        
        // Debug.Log("player speed is " + player.GetComponent<PlayerMovement>().getMoveSpeed());
        bgPosition = gameObject.transform;
        thisY = bgPosition.position.y;
    }
	
	// Update is called once per frame
	void Update () {

        if (camera.GetComponent<SmoothCamera>()._canCameraFollow)
        {
            newposition.x = gameObject.transform.position.x + (player.GetComponent<PlayerMovement>().getMoveSpeed() / 10);
            bgPosition.position = newposition;
            moveBG();
        }

        //Debug.Log(player.GetComponent<PlayerMovement>().getMoveSpeed());
    }


    private void moveBG()
    {

        vec = newposition;
        vec.y = thisY;

        gameObject.transform.position = vec;
            

        
        
    }

}
