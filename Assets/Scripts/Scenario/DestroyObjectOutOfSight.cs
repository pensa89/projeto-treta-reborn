﻿using UnityEngine;
using System.Collections;

public class DestroyObjectOutOfSight : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        AutoDestruction();
    }

    private void AutoDestruction()
    {
        if(  GameObject.Find("Main Camera").transform.position.x >( gameObject.transform.position.x + 20)){
            Destroy(gameObject);
        }
    }
}
