﻿using UnityEngine;
using System.Collections;

public class Dig : MonoBehaviour {

    [SerializeField] private int startTime;

    void OnTriggerStay2D(Collider2D coll)
    {
        if(coll.tag == "Player")
        {
            StartCoroutine("CountdownTimer"); //Corotina é usada para não travar a interface.   
        }        
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.tag == "Player")
        {
            StopCoroutine("CountdownTimer"); 
        }
    }

    IEnumerator CountdownTimer()
    {
        /* 
            Enquanto o player estiver colidindo com a pilha de terra o contador decresce.
        */
        float currentTime = startTime;
        while (currentTime > 0)
        {
            currentTime -= Time.deltaTime;
            yield return 0;
        }
        if (currentTime <= 0)
        {            
            transform.DetachChildren(); //Loot é filho da esfera na hierarquia, esse comando separa os dois.
            Destroy(gameObject); //Destrói somente o pai enquanto o filho permanece na hierarquia.
        }
    }
}
