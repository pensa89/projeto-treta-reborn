﻿using UnityEngine;
using System.Collections;

public class BackGroundCreator : MonoBehaviour {
    //background objects from layer 1
    [SerializeField]private GameObject bg1layer1;
    [SerializeField]private GameObject bg2layer1;
    //[SerializeField]protected GameObject[] bg1Objects = new GameObject[2];
    //background objects from layer 2
    [SerializeField]private GameObject bg1Layer2;

    // Use this for initialization
    void Start() {
        // bg1layer1 = new GameObject();
        //  bg2layer1 = new GameObject();
       // bg1layer1 = GameObject.Find("Level_Layers").GetComponent<LevelOneCreator>().getBGLayer1Obj(0);
        //bg2layer1 = GameObject.Find("Level_Layers").GetComponent<LevelOneCreator>().getBGLayer1Obj(1);
        CreateStartScene();

    }

    // Update is called once per frame
    void Update()
    {
        //nothing
    } 


    void CreateStartScene()
    {
            Instantiate(bg1layer1, new Vector2(0.0f,0.7f), Quaternion.identity);
            Instantiate(bg2layer1, new Vector2(19.2f, 0.7f), Quaternion.identity);
            Instantiate(bg1Layer2, new Vector2(0.0f, 0.7f), Quaternion.identity);


    }

    public void CreateBGLayer1(float x, float y, int currentBGL1)
    {
        //bg1layer1 = bg1Objects[0];
        //bg2layer1 = bg1Objects[1];
        if (currentBGL1 == 0)
        {
            Instantiate(bg1layer1, new Vector2(x+19.2f, y), Quaternion.identity);
            

        }
        else if (currentBGL1 ==1)
        {
            Instantiate(bg2layer1, new Vector2(x + 19.2f, y), Quaternion.identity);
        }

    }
    public void CreateBGLayer2(float x, float y)
    {
        Instantiate(bg1Layer2, new Vector2(x, y), Quaternion.identity);
    }
}
