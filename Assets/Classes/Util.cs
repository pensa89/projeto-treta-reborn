﻿using UnityEngine;
using System.Collections;

public class Util {

    public static void FacePointHorizontally(Transform transform, Vector3 target)
    {
        // The direction sign is positive if the target is on the right of the game object.
        float currentDirectionSign = Mathf.Sign(target.x - transform.position.x);
        // Considering right is forward, a game object is facing left if his rotation.y is around 180 (in degrees) or around 1 or -1 (in radians).
        bool facingRight = Mathf.Abs(transform.rotation.y) == 1 ? false : true;
        bool facingTarget = (facingRight && currentDirectionSign == 1) || (!facingRight && currentDirectionSign == -1);
        if (!facingTarget)
        {
            transform.Rotate(transform.rotation.x, transform.rotation.y + (Mathf.Sign(transform.localScale.x) * 180), transform.rotation.z);
        }
    }
}
